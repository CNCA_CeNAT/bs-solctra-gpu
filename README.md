# BS-SOLCTRA GPU

This repository holds the OpenMP Offloading-based implementation of the BS-SOLCTRA simulator. This is a field line tracing code created to map the magnetic flux surfaces of the SCR-1 stellarator. 

## Getting started

This repository holds three versions of the code. Each version is under a different branch of this project. 

- CPU implementation: MPI + OpenMP + automatic compiler vectorization
- OpenMP GPU prescriptive implementation: MPI + OpenmP offload
- OpenMP GPU descriptive implementation: OpenMP offload based on the target loop pragma

## Compilation

Each implementation is provided with a Makefile. You should load the necessary modules on your system. 
For the CPU version you'll need an Intel Compiler and preferably IMPI. 

For the GPU versions, you'll need either an NVIDIA, AMD or Intel compiler with offloading capabilities. 
A Makefile is provided in each GPU implementation with the different necessary compilation flags for each platform. 


